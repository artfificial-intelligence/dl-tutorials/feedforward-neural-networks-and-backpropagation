# 피드포워드 신경망과 역전파 <sup>[1](#footnote_1)</sup>

> <font size="3">역전파 알고리즘을 사용하여 피드포워드 신경망이 어떻게 훈련되는지 알아본다.</font>

## 목차

1. [개요](./feedforward-neural-networks-and-backpropagation.md#intro)
1. [피드포워드 신경망이란?](./feedforward-neural-networks-and-backpropagation.md#sec_02)
1. [피드포워드 신경망의 동작](./feedforward-neural-networks-and-backpropagation.md#sec_03)
1. [역전파란?](./feedforward-neural-networks-and-backpropagation.md#sec_04)
1. [역전파의 동작](./feedforward-neural-networks-and-backpropagation.md#sec_05)
1. [역전파가 중요한 이유](./feedforward-neural-networks-and-backpropagation.md#sec_06)
1. [역전파의 과제와 한계](./feedforward-neural-networks-and-backpropagation.md#sec_07)
1. [요약](./feedforward-neural-networks-and-backpropagation.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 4 — Feedforward Neural Networks and Backpropagation](https://medium.com/ai-advances/dl-tutorial-4-feedforward-neural-networks-and-backpropagation-053365045077?sk=efad546fceef4b5c53dc4d6721a911f7)를 편역하였다.
